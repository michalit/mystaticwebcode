package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testrDecreasecounterC1() throws Exception {

        final int tempVar= new Increment().decreasecounter(0);
        assertEquals("Add", 0, tempVar);

    }

    @Test
    public void testrDecreasecounterC2() throws Exception {

        final int tempVar= new Increment().decreasecounter(1);
        assertEquals("Add", 0, tempVar);

    }

    @Test
    public void testrDecreasecounterC3() throws Exception {

        final int tempVar= new Increment().decreasecounter(5);
        assertEquals("Add", 1, tempVar);

    }


}

