package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        final int tempVar= new Calculator().add();
        assertEquals("Add", 9, tempVar);

    }

    @Test
    public void testSub() throws Exception {

        final int tempVar= new Calculator().sub();
        assertEquals("Sub", 3, tempVar);

    }
}


